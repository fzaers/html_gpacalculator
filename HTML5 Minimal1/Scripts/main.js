



function SelectGrade(s, c, d)
{
	var g = document.getElementById(s).value;
	var ce = c * g;
	if (isNaN(g))
		ce = "-";
	document.getElementById(d).innerHTML = ce;
	ComputeGPA();
}

function ComputeGPA()
{
	var tc = 0;
	var ce = 0;
	var t = document.getElementById("Modules");

	for (i = 1; i <= 8; i++)
	{
		if (t.rows[i].cells[3].innerHTML != "-")
		{
			var e = parseInt(t.rows[i].cells[3].innerHTML);
			ce += e;
			var c = parseInt(t.rows[i].cells[1].innerHTML);
			tc += c;
		}
	}

	document.getElementById("Credits_Attempted").innerHTML = tc;
	document.getElementById("Credits_Earned").innerHTML = ce;
	var g = (parseFloat(ce) / parseFloat(tc)).toFixed(2);
	document.getElementById("Cumulative_GPA").innerHTML = g;

	if (isNaN(g))
		g = "-";
	else if (g <= 0 && tc >= 51)
		g = (tc / 7).toFixed(3);
	else if (tc == 44 && ce == 101 && CheckCell(1, 3, "-"))
		ClearTable(0);
}

function ClearTable(v)
{
	var t = document.getElementById("Modules");

	for (i = 0; i < t.rows.length; i++)
	{
		for (j = 0; j < t.rows[i].cells.length; j++)
		{
			if(i != 9)
				t.rows[i].cells[j].innerHTML = v;
		}
	}
}

function CheckCell(r, c, v)
{
	var t = document.getElementById("Modules");
	if (t.rows[r].cells[c].innerHTML == v)
		return true;

	return false;
}

window.onload = function ()
{
	document.getElementById("myForm").reset();   	
}